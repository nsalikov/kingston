# -*- coding: utf-8 -*-
import json
import js2xml
import scrapy
from scrapy.shell import inspect_response


class SsdSpider(scrapy.Spider):
    name = 'ssd'
    allowed_domains = ['kingston.com']
    start_urls = ['https://www.kingston.com/unitedkingdom/en/ssd?sortby=nameatz']

    handle_httpstatus_list = [500]

    api_url = 'https://www.kingston.com/unitedkingdom/en/addtocart/ajax/getaddtocart'
    api_headers = {
        'accept-language': 'en-US,en;q=0.5',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'authority': 'www.kingston.com',
        'content-type': 'application/json;',
        'origin': 'https://www.kingston.com',
        'referer': None,
        'user-agent': None,
        'x-requested-with': 'XMLHttpRequest',
    }


    def parse(self, response):
        # inspect_response(response, self)

        items_css = 'ul#section-gallery-1 a.c-productCard2__productPageLink ::attr(href)'
        items = response.css(items_css).extract()

        for item in items:
            yield response.follow(item, callback=self.parse_item)


    def parse_item(self, response):
        # inspect_response(response, self)

        d = {}
        d['url'] = response.url
        d['product_id'] = response.css('section[data-product] ::attr(data-product)').extract_first()

        if not d['product_id']:
            d['product_id'] = ''

        d['specs'] = dict(self.get_specs(response))
        d['filters'] = self.get_filters(response)
        d['variants'] = {}

        if d['filters'] is None:
            return d

        filters = [(k,e) for k,v in d['filters'].items() for e in v]

        if filters:
            capacity, sku = filters.pop(0)
            meta = {'item': d, 'filters': filters, 'capacity': capacity, 'sku': sku}

            headers = self.api_headers
            headers['referer'] = response.url
            headers['user-agent'] = self.settings['USER_AGENT']

            payload = '{"kingstonPN":"%s","productCategory":"","productSubCategory":"","product":%s,"referrer":""}' % (sku, d['product_id'])

            return scrapy.Request(self.api_url, method='POST', headers=self.api_headers, body=payload, meta=meta, callback=self.parse_api, dont_filter=True)
        else:
            return d


    def parse_api(self, response):
        d = response.meta['item']
        filters = response.meta['filters']

        if response.status == 500:
            self.logger.warning('Bad response status: <{}>'.format(response.url))
        else:
            try:
                data = json.loads(response.text)
            except Exception as e:
                self.logger.warning('Unable to parse json: <{}>'.format(response.url), exc_info=True)
                self.logger.debug(response.text)
            else:
                if type(data) is not dict or 'kingstonPN' not in data:
                    self.logger.warning('Unknown data format: <{}>'.format(response.url))
                else:
                    d['variants'][data['kingstonPN']] = {
                        'Capacity': response.meta['capacity'],
                        'kingstonPN': data['kingstonPN'],
                        'SubCategory': data['SubCategory'],
                        'Product': data['Product'],
                        'Price': data['Price']['Price']
                    }

        if filters:
            capacity, sku = filters.pop(0)
            meta = {'item': d, 'filters': filters, 'capacity': capacity, 'sku': sku}

            headers = self.api_headers
            headers['referer'] = response.url
            headers['user-agent'] = self.settings['USER_AGENT']

            payload = '{"kingstonPN":"%s","productCategory":"","productSubCategory":"","product":%s,"referrer":""}' % (sku, d['product_id'])

            return scrapy.Request(self.api_url, method='POST', headers=self.api_headers, body=payload, meta=meta, callback=self.parse_api, dont_filter=True)
        else:
            return d


    def get_filters(self, response):
        script = response.xpath('//script[contains(text(), "KCMS.AddToCart")]/text()').extract_first()

        try:
            jstree = js2xml.parse(script)
            path = '//funcexpr/body/functioncall[child::function/dotaccessor/property/identifier[@name="initialize"]]/arguments/object'
            d = js2xml.make_dict(jstree.xpath(path)[0])
        except Exception as e:
            self.logger.warning('Unable to parse the embedded script: <{}>'.format(response.url), exc_info=True)
            return

        filters = {i['Title']: i['PartNumbers'] for f in d.get('Filters', []) for i in f.get('Items', []) if f.get('Title', None) == 'Capacity'}

        return filters


    def get_specs(self, response):
        for tr in response.css('#specifications .c-table__main tr'):
            key = ' '.join([s.strip() for s in tr.css('tr>td:nth-child(1) ::text').extract()])
            value = [s.strip() for s in tr.css('tr>td:nth-child(2) ::text').extract()]
            yield (key, value)
