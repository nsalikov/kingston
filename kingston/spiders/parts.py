# -*- coding: utf-8 -*-
import re
import json
import scrapy
from jsonfinder import jsonfinder
from scrapy.shell import inspect_response
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode


class PartsSpider(scrapy.Spider):
    name = 'parts'
    allowed_domains = ['kingston.com']

    systems = {
        'Servers': 8,
        'MicroServers': 20,
        'Workstations': 1,
        'Desktop/Tower PCs': 2,
        'Notebook PCs': 3,
        'Ultrabooks': 19,
        'Tablet PCs': 15,
        'Handheld PCs or Devices': 10,
        'Motherboards': 7,
        'NUC': 21,
        'Phones/Smartphones': 14,
        'Printers': 4,
        'Digital Audio Players': 12,
        'Digital Cameras': 11,
        'Video/Action Camera': 18,
        'GPS': 16,
        'Game Consoles': 17,
        'Others': 13,
    }

    product_url = 'https://www.kingston.com/us/memory/search?DeviceType={}&Mfr={}&Line={}&Model={}'
    getmfrbydevice_url = 'https://www.kingston.com/us/memorysearch/ajax/getmfrbydevice'
    getlines_url = 'https://www.kingston.com/us/memorysearch/ajax/getlines'
    getmodels_url = 'https://www.kingston.com/us/memorysearch/ajax/getmodels'

    api_headers = {
        'accept-language': 'en-US,en;q=0.5',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'authority': 'www.kingston.com',
        'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'origin': 'https://www.kingston.com/',
        'referer': 'https://www.kingston.com/us/memory/search',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36',
        'x-requested-with': 'XMLHttpRequest',

    }


    def start_requests(self):
        yield from self.getmfrbydevice()


    def getmfrbydevice(self):
        # inspect_response(response, self)

        for key, value in self.systems.items():
            data = {'systemId': value, 'discontinued': 'false'}
            category = {'System': (key, value)}
            meta = {'category': category, 'data': urlencode(data)}

            yield scrapy.Request(self.getmfrbydevice_url, method='POST', headers=self.api_headers, body=meta['data'], meta=meta, callback=self.getlines)


    def getlines(self, response):
        # inspect_response(response, self)

        data = json.loads(response.text)
        categories = [{'Manufacturer': (d['Text'], d['Value']), **response.meta['category']} for d in data]

        if not data:
            self.logger.info('Skipping: {}'.format(response.meta['category']))
            return

        for category in categories:
            data = {
                'systemCategoryId': category['System'][1],
                'manufacturerId': category['Manufacturer'][1],
                'discontinued': 'false',
            }

            meta = {'category': category, 'data': urlencode(data)}

            yield scrapy.Request(self.getlines_url, method='POST', headers=self.api_headers, body=meta['data'], meta=meta, callback=self.getmodels)


    def getmodels(self, response):
        # inspect_response(response, self)

        data = json.loads(response.text)
        categories = [{'Product Line': (d['Text'], d['Value']), **response.meta['category']} for d in data]

        if not data:
            self.logger.info('Skipping: {}'.format(response.meta['category']))
            return

        for category in categories:
            data = {
                'systemCategoryId': category['System'][1],
                'manufacturerId': category['Manufacturer'][1],
                'systemLine': category['Product Line'][1],
                'discontinued': 'false',
            }

            meta = {'category': category, 'data': urlencode(data)}

            yield scrapy.Request(self.getmodels_url, method='POST', headers=self.api_headers, body=meta['data'], meta=meta, callback=self.parse_models)


    def parse_models(self, response):
        # inspect_response(response, self)

        data = json.loads(response.text)
        categories = [{'Model': (d['Text'], d['Value']), **response.meta['category']} for d in data]

        if not data:
            self.logger.info('Skipping: {}'.format(response.meta['category']))
            return

        for category in categories:
            url = self.product_url.format(category['System'][1], category['Manufacturer'][1], category['Product Line'][1], category['Model'][1])
            meta = {'category': category, 'data': url}

            yield scrapy.Request(url, meta=meta, callback=self.parse_model)


    def parse_model(self, response):
        # inspect_response(response, self)

        category = response.meta['category']
        d = {}

        d['System'] = category['System'][0]
        d['Manufacturer'] = category['Manufacturer'][0]
        d['Product Line'] = category['Product Line'][0]
        d['Model'] = category['Model'][0]
        d['Model URL'] = response.request.url
        d['System Specs'] = dict(self.get_server_specs(response))
        d['Parts'] = list(self.get_parts(response))

        return d


    def get_server_specs(self, response):
        tr_css = '.c-table__main_disable>table>tr'

        for tr in response.css(tr_css):
            key = tr.css('tr>td:nth-child(1) ::text').extract_first().strip()
            key = key.strip().strip(':')
            value = list(filter(None, [text.strip() for text in tr.css('tr>td:nth-child(2) ::text').extract()]))

            yield key, value


    def get_parts(self, response):
        scripts_css = 'script ::text'
        scripts = [s for s in response.css(scripts_css).extract() if 'DataSource:' in s]

        for script in scripts:
            js = list(jsonfinder(script))
            parts_list = js[1][2]

            for p in parts_list:
                yield self.fix_part(p)


    def fix_part(self, part):
        fields_to_delete = [
            'AddToCartExperience',
        ]

        part['BulletPoints'] = part.get('AddToCartExperience', {}).get('BulletPoints', {})

        for f in fields_to_delete:
            if f in part:
                del part[f]

        return part
